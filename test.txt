
testReport :: IO ()
testReport = readJournalFile Nothing Nothing fp >>= either putStrLn (writeReport outf) where
    fp = "C:\\dev\\projects\\finances\\jannsledger.hledger"
    outf = "C:\\dev\\projects\\hledger-reports\\report.html"