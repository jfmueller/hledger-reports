{-# LANGUAGE OverloadedStrings #-}
-- Some functions that are used by reports
module Reports.Common where

import Data.Foldable hiding (concat)
import Data.List (nub, sort)
import qualified Data.Map.Strict as M
import Data.Monoid
import Data.Time.Calendar

import Text.Blaze.Html5 hiding (a, i, style, q)
import Text.Blaze.Html4.Strict.Attributes (align)
import Text.Blaze.Html5.Attributes hiding (name, id)

import Hledger hiding (Reader)

data Oddity = Even | Odd

oddities :: [Oddity]
oddities = cycle [Odd, Even]

-- Render a section of the financial report.
renderReportSection :: 
    (Day -> String) -> -- How to render a date
    String -> -- Name of the section
    Either [Day] [(Day, Day)] -> -- Either a list of dates (for assets/liabilities) or a list of intervals (for income/expenses)
    Journal -> -- Journal with transactions to be used in the report
    Query -> -- Query for the accounts used in the report
    Html
renderReportSection ds sectionName d j q = do
    h2 $ toHtml sectionName
    let days = either (zip (repeat Nothing)) (fmap (\(l, r) -> (Just l, r))) d
    let repDates = fmap snd days
    let balanceRep = flip (balanceReport defreportopts) j
    let theReport = fmap M.fromList $ M.fromList $ fmap (fmap $ fst . balanceRep . flip interv q) $ zip repDates days
    let amountAt n d' = (M.lookup d' theReport) >>= M.lookup n
    renderAccountsTable ds (accountNames theReport) d amountAt

accountNames :: M.Map a (M.Map RenderableAccountName b) -> [RenderableAccountName]
accountNames = sort . nub . concat . M.foldl (flip (:)) [] . fmap M.keys

interv :: (Maybe Day, Day) -> Query -> Query
interv (d1, d2) q = And [q, (Date $ DateSpan d1 $ Just $ succ d2)]

-- The first parameter is a list of account names (table rows)
-- The second parameter is a list of dates (table columns)
-- The third parameter describes the data in each cell
renderAccountsTable :: (Day -> String) -> [RenderableAccountName] -> Either [Day] [(Day, Day)] -> (RenderableAccountName -> Day -> Maybe MixedAmount) -> Html
renderAccountsTable sd names days amounts = table $ (firstRow ! class_ "firstRow") <> foldMap (renderRow amounts) (zip oddities names) where
    firstRow = tr $ td mempty <> (foldMap (td . toHtml) $ either (fmap sd) (fmap (\(f, t) -> sd f ++ " to " ++ sd t)) days)
    reportDays = either (fmap id) (fmap snd) days
    renderRow f (Odd, n)  = (aRow f n) ! (class_ "oddRow")
    renderRow f (Even, n) = (aRow f n) ! (class_ "evenRow") 
    aRow f n = rowStyle n $ tr $ (accountNameToCell n) <> (foldMap (renderCell . f n) reportDays)
    renderCell = rightFloat . td . maybe "-" (toHtml . show) 
    rightFloat a = a ! align "right"

accountNameToCell :: RenderableAccountName -> Html
accountNameToCell n = c ! class_ "firstColumn" where
    c = cellStyle n $ td $ toHtml $ name n

rowStyle :: RenderableAccountName -> Html -> Html
rowStyle (_,_,i) = setStyle where
    setStyle a = case i of
        0 -> a
        1 -> a ! style "padding-left: 5px;  font-size: 10pt;"
        2 -> a ! style "padding-left: 8px;  font-size: 9pt;"
        3 -> a ! style "padding-left: 10px; font-size: 8pt;"
        _ -> a ! style "padding-left: 12px; font-size: 7pt;"

cellStyle :: RenderableAccountName -> Html -> Html
cellStyle (_,_,i) = setStyle where
    setStyle a = case i of
        0 -> a
        1 -> a ! style "padding-left: 5px;"
        2 -> a ! style "padding-left: 20px;"
        3 -> a ! style "padding-left: 30px;"
        _ -> a ! style "padding-left: 40px;"

name :: RenderableAccountName -> AccountName
name (_, n, _) = n

shortDate :: Day -> String
shortDate d = m <> " " <> (show n) where
    (_, m', n) = toGregorian d
    m = case m' of
        1 -> "Jan"
        2 -> "Feb"
        3 -> "Mar"
        4 -> "Apr"
        5 -> "May"
        6 -> "Jun"
        7 -> "Jul"
        8 -> "Aug"
        9 -> "Sep"
        10 -> "Oct"
        11 -> "Nov"
        12 -> "Dec"
        _ -> "Invalid month!"