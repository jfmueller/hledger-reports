{-# LANGUAGE GeneralizedNewtypeDeriving #-}

-- Some data types for reports
module Reports.Types where

import Control.Applicative
import Control.Monad.Reader
import Data.Monoid
import Data.Time.Calendar
import Hledger hiding (Reader)
import Text.Blaze.Html5 (Html)

-- Command-line arguments    
data ReportArgs = ReportArgs { 
    filePath :: FilePath,
    outFile  :: FilePath,
    numIntervals :: Integer,
    snapToFirstOfMonth :: Bool }

-- Some options that are needed during execution
data ReportOptions = ReportOptions {
    args :: ReportArgs,
    reportDays :: [Day], -- balance sheets will be generated for these dates. I&E statements will be generated for the time spans in between
    showDay :: Day -> String
}

newtype Report = Report { unrep :: Journal -> Reader ReportOptions Html }
    deriving Monoid

-- This instance seems to be missing in Control.Monad.Reader (probably for a reason :()) 
-- It is needed for making Report a monoid
instance (Monoid a, Monad m, Applicative m) => Monoid (ReaderT r m a) where
    mempty = return mempty
    mappend a b = (<>) <$> a <*> b

runReport :: Report -> ReportOptions -> Journal -> Html
runReport r op j = runReader (unrep r j) op