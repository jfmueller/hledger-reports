{-# LANGUAGE OverloadedStrings #-}
module Reports.Style where

import Clay
import Data.Monoid

defaultStyle :: Css
defaultStyle = do
	body ? fontFamily [] [sansSerif]
	".firstRow" ? fontVariant smallCaps
