{-# LANGUAGE OverloadedStrings #-}
module Reports(createReport) where

import Clay (render)
import Control.Monad (liftM)
import Control.Monad.Reader
import Data.Monoid
import Hledger hiding (Reader)
import Prelude hiding (head)
import Reports.Common
import Reports.Style
import Reports.Types
import Text.Blaze.Html5

createReport :: ReportOptions -> Journal -> Html
createReport = runReport report where
    report = theDoc <> balanceSheet <> incomeStatement

theDoc :: Report
theDoc = Report $ const $ return $ docTypeHtml $ do
    head $ t <> s where
        t = title "Financial Report"
        s = style $ toHtml $ render defaultStyle

balanceSheet :: Report
balanceSheet = Report $ \j -> do
    ds <- fmap reportDays ask
    d2 <- fmap showDay ask
    let assetSection = renderReportSection d2 "Assets" (Left ds) j (journalAssetAccountQuery j)
    let liabilitiesSection = renderReportSection d2 "Liabilities" (Left ds) j (journalLiabilityAccountQuery j)
    return $ (h1 "Balance Sheet") <> assetSection <> liabilitiesSection

incomeStatement :: Report
incomeStatement = Report $ \j -> do
    days <- fmap (intervals . reportDays) ask
    d2 <- fmap showDay ask
    let incomeSection = renderReportSection d2 "Income" (Right days) j (journalIncomeAccountQuery j)
    let expenseSection = renderReportSection d2 "Expenses" (Right days) j (journalExpenseAccountQuery j)
    return $ (h1 "Income and Expenses") <> incomeSection <> expenseSection

-- Get pairs of elements with their successors
intervals :: Enum a => [a] -> [(a, a)]
intervals l = fmap (\e -> (succ $ fst e, snd e)) $ zip l $ drop 1 l