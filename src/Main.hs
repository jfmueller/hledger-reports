module Main where

import Control.Applicative
import Control.Monad
import Data.Foldable
import Data.Monoid
import Data.Time.Clock
import Data.Time.Calendar (Day, addGregorianMonthsClip, fromGregorian, toGregorian)
import Options.Applicative

import Hledger.Read
import Hledger.Data.Types

import Reports
import Reports.Common (shortDate)
import Reports.Types

import Text.Blaze.Html.Renderer.Pretty

main :: IO ()
main = do
    opt <- getOptions
    let (ReportArgs fp outf _ _) = args opt
    let writeReport o = writeFile o . renderHtml . createReport opt
    let ignoreBalanceAssertions = True
    readJournalFile Nothing Nothing True fp >>= either putStrLn (writeReport outf)

reportArgs :: Parser ReportArgs
reportArgs = ReportArgs <$> journal <*> out <*> number <*> snap where
    journal = strOption (
        long "journal"
        <> metavar "JOURNAL"
        <> help "The journal file from which to generate the report")
    out = strOption (
        long "out"
        <> help "Name of the output (HTML) file"    )
    number = fmap (max 1 . maybe 1 id) $ optional $ option auto (
        long "intervals"
        <> short 'i'
        <> help "Number of intervals for which the report should be generated. Default value: 1.")
    snap = flag True False (
        long "snap"
        <> short 's'
        <> help "Intervals start at 1st of month. If set to false, intervals start at today's day of the month")

programArgs :: ParserInfo ReportArgs
programArgs = info (helper <*> reportArgs)
    ( fullDesc
        <> progDesc "Create a report for a ledger file"
        <> header "hledger-reports -- create HTML reports from (h)ledger files")

getOptions :: IO ReportOptions
getOptions = do
    a@(ReportArgs _ _ n s) <- execParser programArgs
    currentDay <- liftM utctDay getCurrentTime
    let firstReportDay = if s
                         then pred $ firstOfMonth currentDay
                         else currentDay
    let d = reverse $ (:) currentDay $ fmap (flip subtractMonths $ firstReportDay) $ [0..pred n]
    return $ ReportOptions a d shortDate

subtractMonths :: Integer -> Day -> Day
subtractMonths i = addGregorianMonthsClip ((-1) * i)

firstOfMonth :: Day -> Day
firstOfMonth d = fromGregorian year month 1 where
    (year, month, _) = toGregorian d
