# README #

A haskell program for creating financial reports from (h)ledger files. At the moment it only creates a balance sheet and an income/expense statement. Graphics will be added soon. 

### How to use it ###

`cabal-install hledger-reports`

`hledger-reports --journal=<journal file> --out=<output file>`

Command line arguments:

* `--journal` The journal file from which to generate the report
* `--out` where the html output should be written to
* `--intervals`, `-i` Number of intervals (reporting period). Minimum value: 1. For example, if you set `-i 2` then there will be two income/expense statementes and three balance sheets.
* `--snap`, `-s` Whether intervals should start at the 1st of the month. If set to false, intervals start at today's day of the month. 
* `--help` show help text

Example use: `hledger-reports --journal=myjournal.hledger --out report.html`